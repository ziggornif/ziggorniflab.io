---
author: "Matthieu Drouian"
title: "🧰 zsh-pkgmanager-switcher - A ZSH JavaScript package manager switcher plugin"
date: "2023-10-25"
description: ""
tags: [
    "zsh",
    "terminal",
    "js"
]
cover:
  image: "/posts/zsh-logo.webp"
---

If you work daily on JavaScript projects with various package manager configurations (npm, yarn, pnpm ...) and you use a ZSH shell (with or without Oh my ZSH), I just released a ZSH plugin to switch the package manager.

This module allows you to automatically load the right package manager by opening your project

<!--more-->

The plugin is installed as follows:

```bash
git clone <https://github.com/drouian-m/zsh-pkgmanager-switcher> ~/.zsh/zsh-pkgmanager-switcher
```

Then declare t in your `.zshrc` file :

```bash
source ~/.zsh/zsh-pkgmanager-switcher/zsh-pkgmanager-switcher.zsh
```

The complete installation documentation is available here : https://github.com/drouian-m/zsh-pkgmanager-switcher/blob/main/INSTALL.md

## Demo

[![asciicast](https://asciinema.org/a/wXIhL2nZepQwgPfk6Y0Lcwe6v.svg)](https://asciinema.org/a/wXIhL2nZepQwgPfk6Y0Lcwe6v)

## Project link

https://github.com/drouian-m/zsh-pkgmanager-switcher