+++
author = "Matthieu Drouian"
title = "Construire un système de domotique DIY pour le chauffage"
date = "2025-12-31"
description = ""
tags = [
    "domotique"
]
+++

![background](/posts/ernys-EBC4voL9Em0-unsplash.webp)

> Photo by <a href="https://unsplash.com/@ernys?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Ernys</a> on <a href="https://unsplash.com/photos/EBC4voL9Em0?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

<!--more-->

## Introduction

## Étape 1 : Pilotage des radiateurs

## Étape 2 : Intégration des radiateurs dans Home Assistant

## Étape 3 : Création des capteurs de température / humidité

## Étape 4 : Intégration des capteurs dans Home Assistant

## Étape 5 : Scénario de chauffage

## Conclusion

## Ressources
