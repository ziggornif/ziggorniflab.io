---
author: "Matthieu Drouian"
title: "Home Assistant - Créer sa station météo"
date: "2025-01-11"
description: "Guide complet pour créer une station météo personnalisée dans Home Assistant avec prévisions, radar de pluie, alertes météo et suivi des pollens."
tags: ["domotique", "homeassistant", "meteo"]
series: ["Home Assistant"]
lang: "fr"
cover:
  image: "/posts/ha-meteo.webp"
---

Depuis un peu plus d'un an maintenant, j'utilise Home Assistant comme solution de domotique pour automatiser le fonctionnement de mon logement (chauffage, lumières, mesure de la température et de l'humidité, ...).

L'idée m'est venue récemment d'y ajouter une station météo pour pouvoir visualiser rapidement les informations météos utiles au quotidien.

Aujourd'hui nous allons voir comment créer cette station météo avec :

- Prévisions des 5 prochains jours
- Affichage des vigilances en cours
- Radar de pluie
- Alertes sur les pollens pour les risques d'allergies
- Tendances des 7 derniers jours (via un capteur température / humidité zigbee installé à l'extérieur)

## Pré-requis

Pour réaliser ce projet, vous aurez besoin de : 

- Une installation Home Assistant fonctionnelle avec le support des modules complémentaires
- HACS (Home Assistant Community Store) installé [site officiel de HACS](https://hacs.xyz/)

## Installation des modules requis

### Météo France

Tout d'abord, rendons nous sur la page [Intégration Météo France](https://www.home-assistant.io/integrations/meteo_france/) puis cliquer sur le bouton :

![add integration to my home assistant](/posts/config_flow_start.svg)

Une page s'ouvre pour installer l'intégration Météo France dans notre instance Home Assistant.

{{<figimg src="/posts/add-integration-page.webp">}}
Page de redirection vers notre instance Home Assistant.
{{</figimg>}}

Une fois rendu dans notre instance Home Assistant, nous pouvons configurer le module avec la ville de notre choix (dans mon cas Vannes).

{{<figimg src="/posts/ha-meteo-france.webp">}}
Configuration de la ville dans le module météo france sur notre instance Home Assistant.
{{</figimg>}}

C'est ce module qui fournira les informations nécessaires à l'affichage de la carte des prévisions météo.

### Carte Lovelace Météo France

Nous allons maintenant installer la carte des prévisions via un dépôt GitHub HACS.

Pour cela, nous allons ajouter le dépôt [Carte Lovelace Météo France](https://github.com/hacf-fr/lovelace-meteofrance-weather-card) via le menu "Custom repositories" de HACS.

{{<figimg src="/posts/hacs-custom-repo.webp">}}
Bouton pour ajouter le dépôt github de la carte de prévision dans HACS.
{{</figimg>}}

Puis on ajoute les informations suivantes :

- Repository : https://github.com/hacf-fr/lovelace-meteofrance-weather-card
- Type : Dashboard

{{<figimg src="/posts/hacs-add-repo.webp">}}
Modale pour déclarer un nouveau dépôt dans HACS
{{</figimg>}}

Une fois fait, le module peut être installé, cliquer sur le bouton "Download" et suivre les instructions. Le serveur Home Assistant rechargera la configuration une fois installé.

{{<figimg src="/posts/hacs-weather-card-1.webp">}}
Module météo france weather card visible dans la recherche HACS.
{{</figimg>}}

{{<figimg src="/posts/hacs-weather-card-2.webp">}}
Modale de l'installation du module météo france weather card.
{{</figimg>}}

{{<figimg src="/posts/hacs-weather-card-3.webp">}}
Modale indiquant le rechargement de la configuration home assistant.
{{</figimg>}}

### Weather Radar Card

**⚠️ Ce module n'est plus maintenu mais je n'ai pas trouvé d'alternative intéressante pour le remplacer.**

Comme pour le module précédent, nous allons nous rendre dans HACS pour installer le module [Weather Radar Card](https://github.com/Makin-Things/weather-radar-card). Cette fois pas besoin d'ajouter de dépôt, il est disponible dans la liste de modules de base.

{{<figimg src="/posts/hacs-weather-radar-card.webp">}}
Module Weather Radar Card visible dans la recherche HACS.
{{</figimg>}}

On fait donc comme précédemment, téléchargement, installation, recharge de la configuration Home Assistant.

C'est ce module qui nous permettra d'afficher le radar de pluie.

### Pollens home assistant

#### Installation

Passons maintenant au module pour avoir les alertes sur les pollens.

Encore une fois le module sera installé via HACS, comme pour la carte Lovelace Météo France, il faut ajouter le dépôt GitHub [Pollens home assistant](https://github.com/chris60600/pollens-home-assistant).

Cliquer sur le bouton "open hacs repository on my home assistant" présent dans le fichier README, cela ajoutera automatiquement le dépôt dans notre configuration HACS.

![open hacs repository on my home assistant](/posts/open-hacs.svg)

{{<figimg src="/posts/hacs-add-custom-repo.webp">}}
Modale pour ajouter un dépôt dans HACS
{{</figimg>}}

Et encore une fois on fait comme pour les autres modules, téléchargement, installation, recharge de la configuration Home Assistant.

Ce module permettra d'afficher les alertes de pollens pour les allergies.


#### Configuration

Avant de pouvoir l'utiliser, nous allons devoir ajouter l'intégration et configurer les pollens que l'on souhaite afficher.

Pour cela, toujours sur la page HACS du module, cliquer sur le bouton "add integration to my home assistant".

![add integration to my home assistant](/posts/config_flow_start.svg)

Une modale vous demande si vous souhaitez paramétrer le Reseau National de Surveillance Aerobiologique (RNSA), valider pour passer à la configuration.

{{<figimg src="/posts/ha-pollen-setup.webp">}}
Modale pour paramétrer le module de pollens via le Reseau National de Surveillance Aerobiologique (RNSA).
{{</figimg>}}

Sélectionner le département de votre choix.

**⚠️ Important : Penser à décocher l'option "States in literal (in numeric if not selected)" pour avoir les valeurs en numérique. Sans ça les capteurs de pollen afficheront `null` au lieu de `0`.**

{{<figimg src="/posts/ha-pollen-dept.webp">}}
Modale pour paramétrer le choix de département. Avec la case à cocher pour avoir les valeurs de pollen en numérique ou literal.
{{</figimg>}}

On nous demande maintenant de sélectionner les pollens que l'on souhaite suivre.

**⚠️ Étant donné que la liste n'est pas du tout accessible, je vous conseille d'en sélectionner un seul. Nous pourrons activer tous les pollens de notre choix un peu plus loin en allant dans la liste des capteurs.**

{{<figimg src="/posts/ha-pollen-pollens-choice.webp">}}
Modale pour paramétrer le choix des pollens.
{{</figimg>}}

Vous devez obtenir un message de succès, l'affectation du capteur de pollens à une pièce n'est pas spécialement nécessaire ici, dans mon cas j'ai laissé le champ vide. On clique ensuite sur le bouton finish pour terminer la configuration.

#### Activation des pollens et renommage

Vu la difficulté de sélection des éléments dans la liste déroulante des pollens, nous allons nous rendre dans Settings > Devices & services > Pollens (from R.N.S.A website) pour finaliser l'activation des capteurs de pollens que l'on souhaite afficher.

*💡 Nous allons également en profiter pour les renommer. De base chaque pollen a un nom au format `pollens_{code_dpt}_nom` (ex: pollens_56_ambroisies) que nous allons remplacer par le nom du pollen (ex: pollens_56_ambroisies deviendra Ambroisies).*

{{<figimg src="/posts/ha-pollen-config.webp">}}
Page de configuration du module de pollens.
{{</figimg>}}

Sur l'intégration précédemment créée (dans mon cas Pollens Morbihan), cliquer sur le lien `XX entities` pour accéder aux capteurs liés aux pollens. C'est ici que nous allons pouvoir activer et renommer ceux de notre choix.

{{<figimg src="/posts/ha-pollen-config-sensors.webp">}}
Page de configuration du module de pollens.
{{</figimg>}}

Maintenant, pour chaque pollen que vous souhaitez afficher dans le dashboard, procéder de la façon suivante :

- Cliquer sur la ligne du pollen à activer
- Cliquer sur le bouton paramètres présent en haut à droite
- Renommer le pollen
- Cliquer sur "enable" dans le bandeau indiquant que l'entité est désactivée (ou basculer le toggle Enabled à true plus bas dans la modale)

{{<figimg src="/posts/ha-pollen-sensor-enable.webp">}}
Bandeau pour activer le pollen.
{{</figimg>}}

Vous devez avoir un message indiquant que le sensor va être rechargé d'ici 30 secondes. Cliquer ensuite sur le bouton update en bas de la modale de paramétrage.

{{<figimg src="/posts/ha-pollen-sensor-update.webp">}}
La modale de paramétrage d'un pollen avec le message indiquant que le sensor va être rechargé d'ici 30 secondes.
{{</figimg>}}

Répéter l'opération pour tous les pollens à afficher. Dans mon cas je me suis basé sur les informations du [pollinarium de Vannes](https://www.mairie-vannes.fr/pollinarium-sentinelle).

{{<figimg src="/posts/ha-vannes-pollinarium.webp">}}
Capture des informations du pollinarium de Vannes.
{{</figimg>}}

### Composants graphiques supplémentaires

Nous allons avoir également besoin de deux addons graphiques supplémentaires fournis dans la bibliothèque HACS :

- auto-entities : Remplit automatiquement les cartes avec des entités correspondant à certains critères
- bar-card : composant de type barre de progression
- mini-graph-card : composant de graphiques

**L’installation est identique aux installations HACS que nous avons fait précédemment.**

{{<figimg src="/posts/hacs-auto-entities.webp">}}
Composant HACS auto-entities
{{</figimg>}}

{{<figimg src="/posts/hacs-bar-card.webp">}}
Composant HACS bar-card
{{</figimg>}}

{{<figimg src="/posts/hacs-mini-graph-card.webp">}}
Composant HACS mini-graph-card
{{</figimg>}}


## Mise en place de la station météo

Maintenant que toutes les dépendances sont installées, nous pouvons créer notre station météo dans un dashboard Home Assistant. 

Mais avant toute chose nous allons commencer par un reboot de home assistant pour que toutes ces nouvelles dépendances se chargent correctement.

Une fois redémarré, rentrer en édition sur le dashboard de votre choix via le bouton éditer en haut à droite. Dans mon cas je vais créer la station météo dans un onglet dédié mais libre à vous de l'ajouter à un onglet existant 🙂.

{{<figimg src="/posts/ha-new-meteo-tab.webp">}}
Création d'un nouvel onglet météo sur un dasbhoard home assistant.
{{</figimg>}}

### Prévisions météo

Commençons par la carte des prévisions météo, sur le dashboard en édition, cliquer sur l'ajout d'une nouvelle carte puis sélectionner la carte "Carte Météo France par HACF".

{{<figimg src="/posts/ha-dashboard-add-meteofrance.webp">}}
Choix des cartes à ajouter au dashboard et sélection de la carte des prévisions météo.
{{</figimg>}}

Reporter ensuite le paramétrage de la capture suivante en adaptant à la ville que vous avez paramétrée dans le module Météo France.

*💡 Les champs risque de pluie, UV, couverture nuageuse, etc... doivent normalement se configurer automatiquement lors du choix de la ville.*

{{<figimg src="/posts/ha-dashboard-meteofrance-config.webp">}}
Paramétrage de la carte de prévisions météo france.
{{</figimg>}}

Une fois le paramétrage réalisé, vous devez obtenir la carte suivante avec vos prévisions :

{{<figimg src="/posts/ha-dashboard-meteofrance.webp">}}
Carte des prévisions météo france.
{{</figimg>}}

### Radar de pluie

Passons maintenant au radar de pluie.

Comme pour la carte des prévisions, cliquer sur l'ajout d'une nouvelle carte. Cette fois nous allons choisir la carte "Weather Radar Card".

{{<figimg src="/posts/ha-dashboard-add-meteofrance.webp">}}
Choix des cartes à ajouter au dashboard et sélection de la carte Weather Radar Card.
{{</figimg>}}

Reporter une nouvelle fois le paramétrage de la capture suivante.

*💡 Le radar de pluie se base sur la localisation que vous avez définie dans la configuration Home Assistant, si vous souhaitez la changer, vous pouvez utiliser les champs `Marker Latitude` et `Marker Longitude`.*

{{<figimg src="/posts/ha-rain-radar-config.webp">}}
Paramétrage de la carte du radar de pluie.
{{</figimg>}}

De retour sur notre dashboard, il doit maintenant ressembler à ça :

{{<figimg src="/posts/ha-dashboard-rainradar.webp">}}
Dashboard à jour avec les prévisions météo france et le radar de pluie à côté.
{{</figimg>}}

### Alertes sur les pollens

Cette fois, nous allons créer la carte des alertes pollens directement via l'éditeur de code YAML.

*💡 J'ai pu réaliser cette partie grâce au thread suivant sur le forum HACF : [Pollens custom component sensor](https://forum.hacf.fr/t/pollens-custom-component-sensor/3336/44).*

Pour commencer nous allons ajouter une carte de type "Vertical stack".

{{<figimg src="/posts/ha-vertical-stack.webp">}}
Choix des cartes à ajouter au dashboard et sélection de la carte Vertical stack.
{{</figimg>}}

Une fois dedans, basculer en mode code en cliquant sur le bouton "Show code editor" puis coller le code suivant dans l'éditeur :

```yaml
type: vertical-stack
cards:
  - type: gauge
    entity: sensor.pollens_56_risklevel
    needle: true
    min: -0.5
    max: 3.5
    segments:
      - from: -0.5
        color: "#43a047"
      - from: 0.5
        color: "#63d087"
      - from: 1.5
        color: "#ff781f"
      - from: 2.5
        color: "#ff2200"
    name: Risque d'allergie
    severity:
      green: 0
      yellow: 0
      red: 0
  - type: custom:auto-entities
    filter:
      include: null
      template: |
        {%  set ALTNAME = 'd'-%} 
        {% set dpt = 56  %}
        {% set search_string = 'sensor.pollens_'+ dpt|string + '_' %}
        {% set xclude_string = 'sensor.pollens_'+ dpt|string + '_risklevel' %}
        {% for state in states.sensor -%}
          {%- if state.entity_id | regex_match(search_string,ignorecase=False) -%}
            {%- if state.entity_id != xclude_string -%}
               {%- set NAME = state_attr(state.entity_id,"pollen_name") -%}
                {{
                  { 'entity': state.entity_id,
                    'name': NAME
                  } }},
             {%- endif -%} 
          {%- endif -%}
        {%- endfor %}
      exclude:
        - state: < 0
    sort:
      numeric: true
      reverse: true
      method: state
    card:
      type: custom:bar-card
      title: Allergènes en alerte
      min: -0.5
      max: 3.5
      severity:
        - color: "#43a047"
          from: -0.5
          to: 0.5
          hide: true
        - color: orange
          from: 0.5
          to: 1.5
        - color: "#ff781f"
          from: 1.5
          to: 2.5
        - color: "#ff2200"
          from: 2.5
          to: 3.5
      columns: "2"
      animation:
        speed: "1"
      target: "0.5"
    show_empty: false
```

**⚠️ Penser à remplacer les clés `entity: sensor.pollens_56_risklevel` et `dpt = 56` avec vos valeurs.**

Vous devez obtenir le résultat suivant :

{{<figimg src="/posts/ha-pollen-card.webp">}}
Configuration de la carte pollen avec les alertes affichées à côté du code.
{{</figimg>}}

Et de retour sur le dashboard, nous avons maintenant les alertes pollens en plus des prévisions et du radar de pluie.

{{<figimg src="/posts/ha-dashboard-pollen.webp">}}
Dashboard home assistant à jour avec les allertes de pollens.
{{</figimg>}}

### Tendances des 7 derniers jours (nécessite un capteur de température / humidité extérieur)

Pour cette partie, nous allons utiliser les données d'un capteur de température et d'humidité présent physiquement dans mon installation Home Assistant.

Dans mon cas, c'est un capteur zigbee que j'ai fixé à l'extérieur.

Commençons par créer deux nouvelles cartes de type "Mini Graph", une pour la température et l'autre pour l'humidité.

{{<figimg src="/posts/ha-temperature-card.webp">}}
Carte graphique de température sur 7 jours passés avec min et max.
{{</figimg>}}

{{<figimg src="/posts/ha-humidite-card.webp">}}
Carte graphique de humidité sur 7 jours passés avec min et max.
{{</figimg>}}

L'éditeur n'étant pas pris en charge, nous allons directement configurer les cartes en YAML.

**⚠️ Penser à remplacer les clés `entity` avec vos propres capteurs dans le code.**

Pour la température, copier le code suivant :

```yaml
type: custom:mini-graph-card
name: Température
entities:
  - entity: sensor.tz3000_fllyghyj_ts0201_temperature_2
    aggregate_func: min
    name: Min
    color: blue
  - entity: sensor.tz3000_fllyghyj_ts0201_temperature_2
    aggregate_func: max
    name: Max
    color: "#e74c3c"
hours_to_show: 336
group_by: date
line_width: 3
show:
  extrema: true
  name: false
  legend: false
  average: false
hour24: true
grid_options:
  columns: 12
  rows: 5
```

Et le code suivant pour l'humidité :

```yaml
type: custom:mini-graph-card
entities:
  - entity: sensor.tz3000_fllyghyj_ts0201_humidite_2
    aggregate_func: min
    name: Min
    color: blue
  - entity: sensor.tz3000_fllyghyj_ts0201_humidite_2
    aggregate_func: max
    name: Max
    color: "#e74c3c"
name: Humidité extérieure
hours_to_show: 168
group_by: date
line_width: 2
show:
  extrema: true
  name: false
  legend: false
  average: false
hour24: true
grid_options:
  columns: 12
  rows: 5
```

Et voilà ! Nous avons maintenant l'évolution de la température et de l'humidité à l’extérieur sur les 7 derniers jours avec les valeurs min et max.

Retournons une dernière fois sur le dashboard pour voir le rendu final.

## Rendu final

Une fois toutes les cartes ajoutées, nous arrivons au rendu suivant : 

{{<figimg src="/posts/ha-final-dashboard.webp">}}
Carte graphique de humidité sur 7 jours passés avec min et max.
{{</figimg>}}

Avec un peu de travail supplémentaire, on arrive au résultat suivant :

{{<figimg src="/posts/ha-final-dashboard-v2.webp">}}
Carte graphique de humidité sur 7 jours passés avec min et max.
{{</figimg>}}

Et voici le code YAML complet du dashboard :

{{<collapse summary="Code YAML du dashboard">}}
```yaml
 - type: sections
    max_columns: 4
    title: Météo
    path: meteo
    sections:
      - type: grid
        cards:
          - type: heading
            heading: Prévisions
            heading_style: title
            icon: mdi:weather-sunny
          - type: custom:meteofrance-weather-card
            entity: weather.vannes
            name: Vannes
            alertEntity: sensor.56_weather_alert
            detailEntity: sensor.vannes_rain_chance
            cloudCoverEntity: sensor.vannes_cloud_cover
            rainChanceEntity: sensor.vannes_rain_chance
            freezeChanceEntity: sensor.vannes_freeze_chance
            snowChanceEntity: sensor.vannes_snow_chance
            uvEntity: sensor.vannes_uv
            rainForecastEntity: sensor.vannes_next_rain
            alert_forecast: true
            hourly_forecast: false
            number_of_hourly_forecasts: '4'
            animated_icons: true
            details: true
            one_hour_forecast: true
            daily_forecast: true
            wind_forecast_icons: false
            grid_options:
              columns: 24
              rows: auto
          - type: custom:weather-radar-card
            data_source: RainViewer-Original
            zoom_level: 7
            show_marker: true
            show_scale: false
            show_zoom: true
            show_playback: true
            show_range: false
            show_recenter: false
            square_map: false
            extra_labels: false
            grid_options:
              columns: 24
              rows: auto
        column_span: 4
      - type: grid
        cards:
          - type: heading
            heading: Évolution sur les 7 derniers jours
            heading_style: title
            icon: mdi:calendar
            grid_options:
              columns: 24
              rows: 1
          - type: heading
            heading: Alertes pollens
            heading_style: title
            icon: mdi:tree
            grid_options:
              columns: 24
              rows: 1
          - type: custom:mini-graph-card
            name: Température
            entities:
              - entity: sensor.tz3000_fllyghyj_ts0201_temperature_2
                aggregate_func: min
                name: Min
                color: blue
              - entity: sensor.tz3000_fllyghyj_ts0201_temperature_2
                aggregate_func: max
                name: Max
                color: '#e74c3c'
            hours_to_show: 336
            group_by: date
            line_width: 3
            show:
              extrema: true
              name: false
              legend: false
              average: false
            hour24: true
            grid_options:
              columns: 12
              rows: 5
          - type: custom:mini-graph-card
            entities:
              - entity: sensor.tz3000_fllyghyj_ts0201_humidite_2
                aggregate_func: min
                name: Min
                color: blue
              - entity: sensor.tz3000_fllyghyj_ts0201_humidite_2
                aggregate_func: max
                name: Max
                color: '#e74c3c'
            name: Humidité extérieure
            hours_to_show: 168
            group_by: date
            line_width: 2
            show:
              extrema: true
              name: false
              legend: false
              average: false
            hour24: true
            grid_options:
              columns: 12
              rows: 5
          - type: vertical-stack
            cards:
              - type: gauge
                entity: sensor.pollens_56_risklevel
                needle: true
                min: -0.5
                max: 3.5
                segments:
                  - from: -0.5
                    color: '#43a047'
                  - from: 0.5
                    color: '#63d087'
                  - from: 1.5
                    color: '#ff781f'
                  - from: 2.5
                    color: '#ff2200'
                name: Risque d'allergie
                severity:
                  green: 0
                  yellow: 0
                  red: 0
              - type: custom:auto-entities
                filter:
                  include: null
                  template: >
                    {%  set ALTNAME = 'd'-%} 

                    {% set dpt = 56  %}

                    {% set search_string = 'sensor.pollens_'+ dpt|string + '_'
                    %}

                    {% set xclude_string = 'sensor.pollens_'+ dpt|string +
                    '_risklevel' %}

                    {% for state in states.sensor -%}
                      {%- if state.entity_id | regex_match(search_string,ignorecase=False) -%}
                        {%- if state.entity_id != xclude_string -%}
                           {%- set NAME = state_attr(state.entity_id,"pollen_name") -%}
                            {{
                              { 'entity': state.entity_id,
                                'name': NAME
                              } }},
                         {%- endif -%} 
                      {%- endif -%}
                    {%- endfor %}
                  exclude:
                    - state: < 0
                sort:
                  numeric: true
                  reverse: true
                  method: state
                card:
                  type: custom:bar-card
                  title: Allergènes en alerte
                  min: -0.5
                  max: 3.5
                  severity:
                    - color: '#43a047'
                      from: -0.5
                      to: 0.5
                      hide: true
                    - color: orange
                      from: 0.5
                      to: 1.5
                    - color: '#ff781f'
                      from: 1.5
                      to: 2.5
                    - color: '#ff2200'
                      from: 2.5
                      to: 3.5
                  columns: '2'
                  animation:
                    speed: '1'
                  target: '0.5'
                show_empty: false
        column_span: 4
    cards: []
    badges: []
```
{{</collapse>}}


## Ressources

Ci-dessous les ressources qui m'ont aidé pour écrire cet article :

- https://github.com/chris60600/pollens-home-assistant
- https://github.com/hacf-fr/lovelace-meteofrance-weather-card
- https://www.home-assistant.io/integrations/meteo_france/
- https://forum.hacf.fr/t/pollens-custom-component-sensor/3336/44

## Conclusion

Vous avez maintenant une station météo de base intégrée dans votre Home Assistant. De nombreuses améliorations sont possibles comme :

- Personnaliser les composants utilisés
- Ajouter d'autres capteurs physiques (pression atmosphérique, qualité de l'air...)
- Créer des automatisations basées sur les données météo

J'espère que cet article vous aura été utile, en attendant je vous dis à la prochaine avec un autre sujet.