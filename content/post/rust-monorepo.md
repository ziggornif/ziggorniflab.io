+++
author = "Matthieu Drouian"
title = "Create a Rust monorepo with cargo workspace"
date = "2025-09-01"
description = ""
tags = [
    "rust"
]
+++

![background](/posts/jakub-nawrot-9v1cuPQ5hKM-unsplash.webp)

> Photo by <a href="https://unsplash.com/@jacob_lens?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Jakub Nawrot</a> on <a href="https://unsplash.com/photos/9v1cuPQ5hKM?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

<!--more-->

## What's a monorepo ?

A monorepo is a code organization strategy where all the application(s) code is in the same repository.

The idea is to share the whole build part instead of duplicate it in multiple VCS repos.

In a monorepo project we can find :

- applications
- libraries
- configs (code style, build, ...)
- CI

## Is it suitable for my project ?

## Project initialization

## Create server app

## Add internal libraries

## Tooling (Test coverage, CI, ...)

## Conclusion

## Project example

## Useful links

- https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html
