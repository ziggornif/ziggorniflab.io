---
author: "Matthieu Drouian"
title: "Créer son CDN de A à Z"
date: "2024-02-15"
description: ""
tags: [
    "cdn",
    "rust",
]
series: ["Créer son CDN de A à Z"]
lang: "fr"
cover:
  image: "/posts/raoul-droog-yMSecCHsIBc-unsplash.webp"
  caption: "Photo by [Raoul Droog](https://unsplash.com/@raouldroog?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/russian-blue-cat-wearing-yellow-sunglasses-yMSecCHsIBc?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)"
---

## C'est quoi un CDN ?

Un CDN (ou Content Delivery Network) est un réseau de serveur dont le but est de mettre à disposition du contenu sur Internet.

L'objectif principal étant d'optimiser la diffusion des ressources utilisées par les sites et applications web telles que les feuilles de style, les fichiers scripts, les images etc...

Pour les CDN publics, les nœuds vont généralement être répartis à travers le monde. L'objectif étant de répondre à la requête de l'utilisateur avec le serveur géographiquement le plus proche de lui pour réduire la latence.

<!--more-->

## Objectif du projet

L'objectif au fil de cette série d'articles est de montrer comment implémenter son propre CDN étape par étape.

A la différence des CDN publics tels que Cloudflare, l'objectif ici est de créer un CDN qui pourra être auto-hébergé.

Cela peut s'avérer utile au sein d'une entreprise lorsque l'on souhaite pouvoir utiliser certaines ressources sur différents projets web.

## Architecture du CDN

![archi cdn](/posts/cdn-archi.webp)

L'architecture choisie est la suivante :

- Un applicatif écrit en Rust qui jouera le rôle du serveur CDN
- Un object storage respectant l'API S3 pour le stockage des données à héberger
- Un système de cache

Durant le développement, nous utiliserons un container docker [Minio](https://min.io/).

Le choix de l'object storage respectant l'API S3 a plusieurs avantages :

- il offre un vaste choix de fournisseurs cloud à l'international et en France (clevercloud, ovh et scaleway ont des offres d'object storage compatibles S3)
- il permet d'utiliser des librairies communautaires dans la partie applicative pour interagir avec l'object storage (ex: [rust-s3](https://crates.io/crates/rust-s3))

Pour la démonstration, nous utiliserons un système de cache disque local dans un premier temps.

## Fonctionnalités

Concernant les fonctionnalités souhaitées dans notre CDN, l'objectif est d'implémenter ce qu'on retrouve sur les CDN SAAS du marché.

### Upload de modules  

Upload de contenu sous la forme d'archive via une API.

L'idée est de fournir une API à nos utilisateurs pour upload une librairie.

Le format archive sera utilisé pour faciliter l'upload à l'utilisateur. L'extraction des fichiers sera gérée par le code du CDN avant le stockage dans l'object storage.

### Récupération de fichiers

Une API permettant de lire les fichiers stockés dans le CDN.

Par exemple, si l'utilisateur a stocké une librairie `awesome-lib` contenant un fichier `hello-world.js`, il pourra y accéder via l'URL : `/gimme/awesome-lib/hello-world.js`

### Afficher les fichiers d'un module

Le CDN devra disposer d'une interface permettant de visualiser les fichiers d'un module stocké par l'utilisateur.

Pour reprendre l'exemple du point précédent avec la librairie `awesome-lib`, l'URL `/gimme/awesome-lib` affichera l'interface de visualisation du contenu de la librairie avec les fichiers ainsi que leurs tailles.

### Gestion des versions

Le CDN disposera d'un système de gestion de versions semver lors de l'upload.

Il devra également gérer la possibilité de récupérer une version majeure / mineure / patch d'un module.

## Au prochain épisode

Notre architecture étant posée, la prochaine étape sera de lancer un object storage Minio en local.

Une fois en place, nous pourrons développer la couche applicative qui communiquera avec lui.

**Au programme :**

- Lancement de notre object storage de développement
- Initialisation du projet Rust
- Développement de la couche object storage

À la prochaine !

**Lien vers la partie 2 : https://blog.ziggornif.xyz/post/cdn-part2/**
