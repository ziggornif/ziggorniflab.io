---
author: "Matthieu Drouian"
title: "Home Assistant - Récupérer la consommation d'un compteur Linky avec l'API Enedis"
date: "2025-01-19"
description: "Guide complet pour intégrer les données de consommation d'un compteur Linky dans Home Assistant via l'API Enedis. Solution pour les compteurs inaccessibles ou installations sans modification matérielle."
tags: ["domotique", "homeassistant", "linky"]
series: ["Home Assistant"]
lang: "fr"
cover:
  image: "/posts/thomas-kelley-xVptEZzgVfo-unsplash.webp"
  caption: "Photo by [Thomas Kelley](https://unsplash.com/@thkelley?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/gray-ge-volt-meter-at-414-xVptEZzgVfo?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)"
      
---

Aujourd'hui, nous allons voir comment récupérer sa consommation électrique dans Home Assistant via l'API Enedis.

Le compteur Linky est le compteur électrique communicant déployé par Enedis en France. Il permet notamment de suivre sa consommation électrique à distance.

Cette solution peut s'avérer bien pratique si vous vivez en appartement et que votre compteur Linky est situé dans les parties communes, hors de portée de votre réseau domestique.

C'est une solution qui peut également être envisagée si vous êtes locataire et que vous ne souhaitez pas apporter de modification au compteur de votre logement.

**⚠️ Important : cette solution permet de récupérer la consommation électrique à J+1, vous n'aurez pas votre consommation en temps réel.**

## Pré-requis

Pour réaliser ce projet, vous aurez besoin de : 

- Une installation Home Assistant fonctionnelle
- Un compteur électrique Linky
- Un compte [Enedis](https://www.enedis.fr/)

## Enedis - Activer le partage de données

Aller dans la section [Gérer l'accès à mes données](https://mon-compte-particulier.enedis.fr/donnees/) de votre compteur.

**⚠️ L'activation peut prendre plusieurs jours.**

Activer ensuite "Enregistrement de la consommation horaire" et "Collecte de la consommation horaire".

{{<figimg src="/posts/enedis-collect.webp">}}
Page de gestion de l'accès aux données d'un compteur dans Enedis. La capture montre l'activation de "Enregistrement de la consommation horaire" et "Collecte de la consommation horaire".
{{</figimg>}}

Ces options permettrons la récupération des informations par API.

## Installation du module ha-linky

Le module Home Assistant Linky est disponible sur GitHub : [ha-linky](https://github.com/bokub/ha-linky).

Celui-ci permet de synchroniser les données de votre compteur Linky avec les tableaux de bord énergie de Home Assistant.

Tout d'abord, nous allons suivre le readme et cliquer sur le lien pour ajouter le module dans notre instance de Home Assistant.

Lien pour ajouter le module : [Intégrer ha-linky dans Home Assistant](https://my.home-assistant.io/redirect/supervisor_add_addon_repository/?repository_url=https%3A%2F%2Fgithub.com%2Fbokub%2Fha-linky)

La page suivante va s'ouvrir pour ajouter le module :

{{<figimg src="/posts/add-integration-page.webp">}}
Page de redirection vers notre instance Home Assistant.
{{</figimg>}}

Cliquer sur "Open Link", vous devez être redirigé dans votre Home Assistant avec la modale d'ajout du module. Cliquer sur "Ajouter" pour ajouter le dépôt à la liste des sources de modules complémentaires.

{{<figimg src="/posts/ha-add-halinky.webp">}}
Page pour ajouter le dépôt ha-linky dans les modules complémentaires avec l'URL github du dépôt.
{{</figimg>}}

Nous allons maintenant installer le module. Pour cela, rechercher le module `Linky` dans la liste des modules complémentaires puis l'installer.

{{<figimg src="/posts/ha-install-halinky.webp">}}
Page des modules complémentaires filtrée sur le module ha-linky.
{{</figimg>}}

L'installation est terminée, nous allons pouvoir passer à la configuration !

## Récupération des tokens API et configuration du module ha-linky

### Token API

Le module fourni dans sa documentation GitHub une page très bien faite pour récupérer un token API.

Nous allons nous rendre sur la page https://conso.boris.sh/exemples/ puis cliquer sur le lien "J'accède à mon espace client Enedis".

{{<figimg src="/posts/enedis.png">}}
Bouton "J'accède à mon espace client Enedis"
{{</figimg>}}

Une fois dans votre espace Enedis, on vous demande de valider le partage de vos données avec le service Conso-API

{{<figimg src="/posts/enedis-partage.webp">}}
Page pour partager les données avec Conso API
{{</figimg>}}

La validation faite, vous devez obtenir le token d'API ainsi qu'un rappel du numéro PRM de votre compteur.

{{<figimg src="/posts/ha-api-token.webp">}}
Page de confirmation de partage des données avec un token API ainsi que le rappel du PRM du compteur.
{{</figimg>}}

Nous allons pouvoir utiliser ces informations pour configurer la collecte des données.

### Configuration du module ha-linky

Pour configurer la collecte, nous allons nous rendre dans l'onglet "Configuration" du module ha-linky installé sur notre instance Home Assistant.

Dans la section `Options > meters`, nous allons coller le bloc suivant :

```yaml
- prm: "votre code PRM"
  token: >-
    votre token API
  name: Linky consumption
  action: sync
  production: false
```

Explication :

- `prm` : Le code d'identification unique de votre compteur (visible sur votre facture ou l'interface Enedis)
- `token` : Le jeton API généré précédemment
- `name` : Le nom qui sera affiché dans Home Assistant pour cette source d'énergie
- action : Le type d'action à effectuer (`sync` pour la synchronisation, `reset` pour réinitialiser les données)
- production : À définir sur `true` si vous produisez de l'électricité (panneaux solaires par exemple)

**⚠️ Si vous avez une production d'électricité, vous pouvez ajouter un bloc de configuration avec le paramètre `production` à `true`.**

Vous pouvez également configurer le prix du kWh dans la section `Options > costs`.

La configuration des coûts permet de définir différents tarifs selon les plages horaires (par exemple pour les heures creuses/pleines). 

Voici à quoi resemble la configuration pour mon logement :

```yaml
- price: 0.2047
  after: "01:34"
  before: "07:34"
- price: 0.2816
  after: "07:34"
  before: 12h34
- price: 0.2047
  after: "12:34"
  before: "14:34"
- price: 0.2816
  after: "14:34"
  before: "01:34"
```

Voici à quoi ressemble le paramétrage complet :

{{<figimg src="/posts/ha-linky-config.webp">}}
Ecran de configuration du module ha-linky avec la configuration API ainsi que la configuration des tarifs horaires
{{</figimg>}}

Une fois la configuration effectuée, le module ha-linky va pouvoir synchroniser la consommation de notre compteur par API.

## Configuration du tableau de bord énergie

Rendons nous dans l'onglet "Énergie" de Home Assistant.

Si vous n'avez pas encore configuré de sources de consommation ou production, vous devez vous retrouver devant cet écran :

{{<figimg src="/posts/ha-energy-default.webp">}}
Page d'accueil du tableau de bord énergie
{{</figimg>}}

Nous allons maintenant ajouter notre source de consommation d'électricité. Cliquer sur "Ajouter une consommation" puis sélectionner la source Linky créée précédemment.

Dans mon cas, j'utilise l'entité `costs` de la source de données pour suivre les coûts vu que j'ai paramétré les tarifs du kWh en fonction des plages horaires.

{{<figimg src="/posts/ha-energy-linky.webp">}}
Modale de paramétrage de la source de commation d'électricité basée sur les données du compteur linky
{{</figimg>}}

## Visualisation de la consommation éléctrique

Maintenant que notre source de consommation électrique est paramétrée dans le tableau de bord énergie, nous pouvons visualiser la consommation de notre compteur Linky directement depuis Home Assistant !

{{<figimg src="/posts/ha-energy-dashboard.webp">}}
Tableau de bord avec les graphiques de consommation d'électricité.
{{</figimg>}}

Si vous souhaitez également avoir l'empreinte carbone du réseau électrique affiché avec votre consomation, il suffit d'activer "Electricity Maps dans l'écran de configuration du tableau de bord énergie.

{{<figimg src="/posts/ha-elec-maps.webp">}}
Configuration de electricy maps
{{</figimg>}}

## Resources

- https://github.com/bokub/ha-linky

## Conclusion

Nous avons vu aujourd'hui comment récupérer notre consommation électrique via l'API Enedis. Cette solution à l'avantage de fonctionner avec tous les compteurs Linky, quelle que soit leur localisation.

Son inconvénient reste la disponibilité des informations à J+1 mais dans mon cas d'usage cela ne pose pas de problème.

Si votre compteur Linky est à porté de réseau, il existe des solutions physique comme [le module ZLinky](https://lixee.fr/produits/42-75-zlinky-tic-v2-3770014375179.html) qui vont se brancher directement sur sortie TIC du compteur pour fournir les informations en temps réel.
