---
author: "Matthieu Drouian"
title: "🦊 🐳 Publish your docker images in a private registry with GitLab Container Registry"
date: "2022-01-16"
description: ""
tags: [
  "gitlab",
  "docker",
]
cover:
  image: "/posts/docker-registry-tiny.webp"
---

It may happen that you need a private registry to publish your docker images (business projects, dev docker images, etc...).

There are several solutions to do that :
- take a docker hub subscription which allows you to create private projects
- create and maintain your own registry (ex: [harbor](https://goharbor.io/))
- use GitLab Registry

The docker hub solution may seem to be the right one for a company with large projects, but for simple needs (ex: management of development docker images) or for side projects, the Gitlab Registry solution is in my opinion the simplest.

<!--more-->

## GitLab Registry ?

GitLab offer a free docker registry for each project whether public or private.

You can access to this registry from your GitLab project interface.

![registry](/posts/gitlab-registry.webp)

Let's take a look to this page.

![registry-2](/posts/gitlab-registry-2.webp)

You may notice that the page says "there are no docker images in the registry at the moment".

This page also provides us with the commands to authenticate, build and post images to this docker registry.

The easiest way (in my opinion) to use this registry is to create a GitLab CI job which will build and publish the project's docker image for us. 

Let's do that !

## GitLab CI Job

In this example, I will use the following Dockerfile : 

```yml
FROM golang:1-alpine as builder
RUN apk update && apk add gcc make g++
WORKDIR /build
ADD . .
RUN make build

FROM alpine
COPY --from=builder /build/gitlab-docker-example /bin/gitlab-docker-example
RUN chmod +x /bin/gitlab-docker-example

# Add static content
COPY --from=builder /build/public ./public

ENV GIN_MODE=release

ENTRYPOINT ["/bin/gitlab-docker-example"]
```

Create a new `.gitlab-ci.yml` file (or add this job to yours if you already have one) in your project.

```yml
docker:
  stage: 🐳 docker
  image: docker
  services:
    - docker:dind
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/ziggornif/gitlab-docker-example:latest .
    - docker push $CI_REGISTRY/ziggornif/gitlab-docker-example:latest
```

## Explanations
`docker login` will authenticate the CI job to the project registry with CI credentials.

`docker build` will build the docker image.

`docker push` will publish to the project registry.

We use the `docker:dind` service because we are using docker commands in a docker container (Gilab Jobs are runned in docker containers).

`$CI_REGISTRY` variable correspond to the the GitLab Registry (registry.gitlab.com).

The project registry URL will always be in the following format : `<registry_url>/<group>/<project_name>`

## First pipeline

Let's commit the previous `.gitlab-ci.yml` in your project. A pipeline should be triggered.

![pipeline](/posts/pipeline-registry.webp)

![pipeline](/posts/pipeline-registry-2.webp)

Take a look to this pipeline, you can see that the docker job has build and published a docker image to the project registry.

![pipeline](/posts/pipeline-registry-3.webp)

## Back to the registry

Now that we have build our first image, go back to the project registry page.

Congratulation, your first image is here 🥳 !

![image](/posts/image-registry.webp)

![image](/posts/image-registry-2.webp)

## Use published images

First, you need to authenticate to the project registry. To do that, you will need a GitLab personal token.

To create one, follow the official guide here : https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

⚠️ You must check the `read_registry` option during token creation.

![image](/posts/token-registry.webp)

Once the token has been created, you can authenticate to the registry.

```shell
docker login registry.gitlab.com -u <username> -p <token>
```

And once authenticated, you will be able to retrieve the docker images of the project.

Example : 

```shell
docker run -p 8080 registry.gitlab.com/ziggornif/gitlab-docker-example:latest
```

That's it ! You can now use Gitlab Registry with your projects 🐳.

## Project example

You can retrieve and fork the complete project here : https://gitlab.com/ziggornif/gitlab-docker-example

## Useful links

https://docs.gitlab.com/ee/user/packages/container_registry/

https://docs.gitlab.com/ee/user/packages/container_registry/#build-and-push-by-using-gitlab-cicd

