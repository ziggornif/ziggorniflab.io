---
author: "Matthieu Drouian"
title: "🔥 express-chaos-middleware - A simple chaos testing lib for Node.JS Express applications"
date: "2021-12-18"
description: ""
tags: [
    "nodejs",
    "express",
    "testing",
]
cover:
  image: "/posts/fire.webp"
---

I just released a small library to put some troubles in my Node.JS Express applications.

https://github.com/drouian-m/express-chaos-middleware

<!--more-->

## How to use it ?

Add the dependency to the project.

```sh
npm add express-chaos-middleware
```

Then load the module in the express application :

```js
const { chaos } = require('express-chaos-middleware');

app.use(chaos());
```

After that the chaos module will be triggered on every route call and goes randomly:
- slow down the application
- return http errors
- throw exceptions 

Probabilities of having issues and the randomly triggered effects can be configured with the input param object ((see documentation))[https://github.com/drouian-m/express-chaos-middleware#usage]

## Effects

Here are some results of library effects observable with a Grafana dashboard.

{{<figimg src="/posts/graph2.webp">}}
This picture is a screenshot of a Grafana dashboard.
In this dashboard, there are four graphs: average routes response time, number of requests per minute, number of failed requests over time grouped by 4xx and 5xx codes and the last one is the overlay of valid and error requests.
This dashboard demonstrates that when the chaos module is enabled, it creates latency and http errors.
We can see the latency and the number of failed requests growing on the graphs.
{{</figimg>}}