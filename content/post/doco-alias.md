---
author: "Matthieu Drouian"
title: "🐳 doco - A shell alias for docker compose"
date: "2022-01-15"
description: ""
tags: [
    "shell",
    "docker",
]
cover:
  image: "/posts/doco-alias.webp"
---

During the last 6 years that I had to use docker, I quickly added a `doco` alias for `docker-compose` (then `docker compose`) in my terminal.

<!--more-->

For example :

`docker compose up -d` becomes `doco up -d`

`docker compose ps` becomes `doco ps`


Today I finally took the time to package it for those who would like to use it.

If you are interested, follow the github project instructions : https://github.com/drouian-m/doco

Have a nice week-end !

