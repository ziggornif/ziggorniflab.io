# blog.ziggornif.xyz

Zig' Blog sources

## Clone

```sh
git clone git@gitlab.com:ziggornif/ziggornif.gitlab.io.git
```

## Hugo

This blog is powered by [Hugo Framework](https://gohugo.io/).


## Theme

I use the PaperMod theme available here with some personal modifications : https://github.com/adityatelange/hugo-PaperMod/

## Configuration

The blog configuration is in the [config.yml](./config.yml) file.

## Add a post

All posts are created in the [content/post](./content/post) directory.

## Run locally

```
hugo server -D
```

## Deploy

The blog is currently deployed automatically from the main branch with Gitlab CI and Gitlab Pages.

See : [.gitlab-ci.yml](./.gitlab-ci.yml)
